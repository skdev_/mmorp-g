#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <argp.h> // Argument parsing

// Networking code copied from:
// https://mohsensy.github.io/programming/2019/09/25/echo-server-and-client-using-sockets-in-c.html

// Argument Parsing code from:
// http://www.gnu.org/software/libc/manual/html_node/Argp-Example-3.html

typedef struct {
  // IPv4 address for our server (Localhost is 127.0.0.1)
  char* server_ip;

  // Server port.
  int   server_port;
} ClientArgs;

static ClientArgs user_args;

// Used by main to communicate with parse_opt
struct arguments {
  char* args[2]; // arg1 & arg2
};

error_t
parse_opt(int key, char* arg, struct argp_state* state)
{
  struct arguments *arguments = state->input;

  switch(key)
  {
  case 'd':
    arguments->args[0] = arg;
    break;
  case 'p':
    arguments->args[1] = arg;
    break;
  default:
    return ARGP_ERR_UNKNOWN;
  }
  return 0;
}

static struct argp_option options [] = {
  // (stirng) Name, (int)Key , (string) argname, (int) flags, (string) doc
  {"destination", 'd', "IP", 0, "Destination/Server IP Address"},
  {"port", 'p', "PORT", 0, "Destination/Server Port"},
  {0}
};


static const char args_doc[] = "ARG1 ARG2";
static const char program_doc[] = "Client to the Echo Server.";

/* Our argp parser. */
static struct argp argp = { options, parse_opt, args_doc, program_doc };

int main(int argc, char** argv)
{
  setbuf(stdout, NULL);
  struct arguments arguments;
  printf("Argc: %d\n", argc);
  error_t error = argp_parse(&argp, argc, argv, 0, 0, &arguments);
  if(error != 0) {
    printf("Error!\n");
  }

  // Convert args into server/port combo.
  user_args.server_ip = arguments.args[0];
  user_args.server_port= atoi(arguments.args[1]);
  printf("(Destination, Port): (%s, %d)\n", user_args.server_ip, user_args.server_port);


  // Set up connection to server.
  int server_fd = socket(AF_INET, SOCK_STREAM, 0);
  if (server_fd < 0 )
  {
    perror("Cannot create socket;\n");
    exit(1);
  }

  struct sockaddr_in server;
  server.sin_family = AF_INET;

  in_addr_t address = inet_addr(user_args.server_ip);
  if(address == INADDR_NONE)
  {
    perror("Cannot parse IPv4 Address\n");
    exit(2);
  }
  server.sin_addr.s_addr = address;
  server.sin_port = htons(user_args.server_port);

  if(connect(server_fd, (struct sockaddr*) &server, sizeof(server)) < 0)
  {
    perror("Cannot connect to server");
    exit(3);
  }

  // Message to send to server.
  const char* message = "Hello, World!";

  if(write(server_fd, message, strlen(message)) < 0)
  {
    perror("Cannot write");
    exit(4);
  }

  char recv[1024];
  memset(recv, 0, sizeof(recv));
  if(read(server_fd, recv, sizeof(recv)) < 0)
  {
    perror("Cannot Read");
    exit(5);
  }

  printf("Received Message \"%s\" from server!\n", recv);
  close(server_fd);

  return 0;
}
