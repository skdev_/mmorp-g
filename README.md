
# Requirements
meson >=0.57.0 (lower this if confirmed working with older versions)

# Building
```
$ meson build
$ ninja -C build
```

# Running
```
$ ./build/mmorpg
```
